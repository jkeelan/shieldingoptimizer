import sys
import glob
import numpy as np
from functools import partial, update_wrapper
from collections import OrderedDict
import os
import pandas as pd
import pathlib
MAX_PX_X = 600
MAX_PX_Y = 600
FILTERS = None
DEFAULTBINS = np.logspace(np.log10(0.1), np.log10(15), 25)
PARTICLECOLORS = {'proton': 'crimson',
                  'protons': 'crimson',
                  'neutron': 'forestgreen',
                  'electron': 'steelblue',
                  'gamma': 'goldenrod',
                  'CXB': 'goldenrod',
                  'electrons': 'steelblue',
                  'alpha': 'forestgreen',
                  'Total Background': 'k',
                  'total': 'k'}

def wrapped_partial(func, *args, **kwargs):
    partial_func = partial(func, *args, **kwargs)
    update_wrapper(partial_func, func)
    return partial_func

def errorpct(x):
    return 100/np.sqrt(np.count_nonzero(x))

def total_count(x):
    return np.count_nonzero(x)

def normalizedcount(norm, x):
    return total_count(x)/norm

def default_filter_data(ddict):
    df = ddict['data']
    dft = filter_data(df, wfi_filter())
    dft = dft[dft['ptype'] != 'proton']
    return dft

def filter_collection(ddict, erange=None, **kwargs):
    """
    Return a dictionary with filters data frames (extended energy range 0.1 - 15 keV)
    """
    rdata = OrderedDict()
    dffilter = wfi_filter()
    if(erange):
        dffilter['total'] = erange
    for name, data in ddict.items():
        df = data['data']
        dft = df
        #dft = filter_data(df, dffilter)
        rdata[name] = dft
    return rdata

def normalize_spectra(spectra_dict, norm_dict):
    for name, pdict in spectra_dict.items():
        norm_factor = norm_dict[name]
        for ptype, hist in pdict.items():
            hist[:,1] *= norm_factor
    return spectra_dict
        

def get_spectra(simdict, bins=DEFAULTBINS, **kwargs):
    rdata = OrderedDict()
    
    if 'particles' in kwargs:
        particles = kwargs['particles']
    else:
        particles = ['proton', 'electron', 'gamma', 'neutron']

    filtered_dfs = filter_collection(simdict, **kwargs)
    
    for name, ddict in simdict.items():
        nprims = ddict['primaries']
        df = filtered_dfs[name]
        pdict = OrderedDict()
        total = np.zeros((len(bins[1:]), 2))
        total[:,0] = bins[1:]
        for ptype in particles:
            dff = df[df['ptype'] == ptype]
            hist = get_histogram(dff['total'], bins=bins, **kwargs)
            hist[:,1] = hist[:,1]/nprims
            total[:,1] += hist[:,1]
            pdict[ptype] = hist
        pdict['total'] = total
        rdata[name] = pdict
        
    return rdata
            

def get_stats(ddict):
    df = ddict['data']
    normcounts = wrapped_partial(normalizedcount, ddict['primaries'])
    dft = filter_data(df, wfi_filter())
    dft = dft[dft['ptype'] != 'proton']
    dfp = pd.pivot_table(dft, index=['ptype'], values=['total'], aggfunc=[normcounts, errorpct, np.mean], fill_value=0)
    return dfp.sort_values([('normalizedcount', 'total')], ascending=False)

def get_histogram(energies, bins=DEFAULTBINS, **kwargs):
    dat, be = np.histogram(energies, bins=bins)
    dat = dat/(be[1:] - be[:-1])
    return np.array(list(zip(be[1:], dat)))
    

## Processing and report generation
def simulation_countrates_ptype(simdict):
    ptype_dict = {}
    ptype_dict['electron'] = simulation_countrates(simdict, ptype='electron')
    ptype_dict['gamma'] = simulation_countrates(simdict, ptype='gamma')
    return ptype_dict

def simulation_countrates(simdict, ptype=None, keys=None):
    """Generate np array of simulation parameters, count rate (2-7 keV) and errors
    simdict should be an ordered dict if keys are provided
    """
    #simdict[item]['numericalkey'] == x axis
    simiter = iter(simdict.keys())
    if(keys):
        for key in keys:
            k = next(simiter)
            simdict[k]['numericalkey'] = key
        
    grid = []
    for sim, ddict in simdict.items():
        df = default_filter_data(ddict)
        if(ptype):
            df = df[df['ptype'] == ptype]
        crate = len(df)/ddict['primaries']
        err = 1.0/np.sqrt(len(df))
        print(sim)
        grid.append([float(ddict['numericalkey']), crate, err*crate])
    grid = np.array(grid)
    grid = grid[np.argsort(grid[:,0]), :]
    return grid
        

def total_counts(ddict, ptype):
    n_prims = float(ddict['primaries'])
    df = ddict['data']
    dft = df[df['ptype'] == ptype]
    n_counts = len(dft)
    return float(n_counts)/n_prims

# the standard wfi filter
def wfi_filter():
    return  {
        'total': [2, 7],
        'valid': True,
    }


def filter_data(df, filters):
    """function for filtering a series of numeric or boolean columns"""
    dft = df
    for col, vrange in filters.items():
        if isinstance(vrange, list) or isinstance(vrange, tuple):
            dft = dft[(dft[col] >= vrange[0]) & (dft[col] < vrange[1])]
        elif isinstance(vrange, bool):
            dft = dft[dft[col] == vrange]
    return dft

def set_global_filters(filters):
    global FILTERS
    FILTERS = filters

#header:
#ptype, eid, particleid, parentid, cproc, vx, vy, vz, x, y, z, ecur, edep, eprimary, enonion, vertexvname, steplen

def parse_pixels(pixels):
    pixels = [px.split(",") for px in pixels]
    pixels = [[int(x[0]) + MAX_PX_X, int(x[1]) + MAX_PX_Y, float(x[2]), int(float(x[3]))] for x in pixels if float(x[2]) > 1e-14]
    #pixels = np.array([np.array(row) for row in pixels])
    return pixels

def prim_row(pr):
    prim_dict = {
        'vertex': np.array([float(pr[5]), float(pr[6]), float(pr[7])]),
        'pos': np.array([float(pr[8]), float(pr[9]), float(pr[10])]),
        'volume': str(pr[-2]),
        'cproc': str(pr[4]),
        'steplen': float(pr[-1]),
        'ptype': str(pr[0]),
        'eid': int(float(pr[1])),
        'pid': int(pr[2]),
        #'eid': int(float(pr[1])),
        #'pid': int(float(pr[2])),
        'eprimary': float(pr[13]),
        'enonion': float(pr[14]),
        'ecur': float(pr[11]),
        'edep': float(pr[12]),
        'deps': [],
        'valid': False,
        'total': 0
    }
    return prim_dict

def parse_primaries(prims):
    prims = [p.split(",") for p in prims]
    prims = [prim_row(pr) for pr in prims]
    prim_dict = {}
    for pr in prims:
        prim_dict[pr['pid']] = pr
    return prim_dict

def get_event_image(df, eid, sim_id):
    dft = df[(df['eid'] == eid) & (df['simulation'] == sim_id)]
    deps = []
    n_events = 0
    for deplist in dft['deps']:
        for dep in deplist:
            n_events += 1

    deps = np.zeros((n_events, 4))
    count = 0
    for deplist in dft['deps']:
        for dep in deplist:
            deps[count, :] = dep
            count += 1
    print(deps)
    return get_image(np.array(deps))

def multievent_image(df, min_eid=0, max_eid=1e6):
    dft = df[(df['eid'] > min_eid) & (df['eid'] < max_eid)]
    deps = []
    n_events = 0
    for deplist in dft['deps']:
        for dep in deplist:
            n_events += 1

    deps = np.zeros((n_events, 4))
    count = 0
    for deplist in dft['deps']:
        for dep in deplist:
            deps[count, :] = dep
            count += 1
    return get_image(np.array(deps))

def multievent_image_bytime(df, min_time=0, max_time=4):
    dft = df[(df['sim_time'] > min_time) & (df['sim_time'] < max_time)]
    deps = []
    n_events = 0
    for deplist in dft['deps']:
        for dep in deplist:
            n_events += 1

    deps = np.zeros((n_events, 4))
    count = 0
    for deplist in dft['deps']:
        for dep in deplist:
            deps[count, :] = dep
            count += 1
    return get_image(np.array(deps))

def get_image(px):
    px = np.array([np.array(row) for row in px])
    xmax, xmin = (np.max(px[:,0]), np.min(px[:,0]))
    ymax, ymin = (np.max(px[:,1]), np.min(px[:,1]))
    sz_x, sz_y = ((xmax - xmin), (ymax - ymin))
    im = np.zeros((int(sz_x) + 6, int(sz_y) + 6))
    for dep in px:
        im[int(dep[0]) - int(xmin) + 2, int(dep[1]) - int(ymin) + 2] += dep[2] #offset pixels
    return im

def find_valid_events(px):
    """px is x,y,dep,primary_id
    return a list of primary id's which led to a valid event
    as well as the event energy"""
    px = np.array([np.array(row) for row in px])
    xmax, xmin = (np.max(px[:,0]), np.min(px[:,0]))
    ymax, ymin = (np.max(px[:,1]), np.min(px[:,1]))
    sz_x, sz_y = ((xmax - xmin), (ymax - ymin))
    im = np.zeros((int(sz_x) + 6, int(sz_y) + 6))
    valid_px = []
    for dep in px:
        im[int(dep[0]) - int(xmin) + 2, int(dep[1]) - int(ymin) + 2] += dep[2] #offset pixels
    for i, row in enumerate(im):
        for j, px in enumerate(row):
            if px == 0:
                continue #stupid
            try:
                impatch = im[i-2:i+3, j-2:j+3]
                if px != np.max(impatch):
                    continue
                edges = np.copy(impatch)
                edges[1:-1,1:-1] = 0
                if np.count_nonzero(edges) > 0:
                    continue
                valid_px.append([i + xmin - 2, j + ymin - 2, np.sum(impatch)])
            except:
                print("EXCEPTION")
                continue
    return valid_px
        

def max_protons(events):
    max_eid = 0
    for e in events:
        if e['eid'] > max_eid:
            max_eid = e['eid']
    return max_eid

def default_name_parser(fn):
    return int(fn.split("_")[0])

def default_prefix_parser(fn):
    return int(fn.split("_")[1])

def parse_collection(folders, sim_parse=default_name_parser, **kwargs):
    """folders is a dictionary of simulation names and directories"""
    sims = OrderedDict()
    for f, di in folders.items():
        sims[f] = parse_folder(di, sim_parse=sim_parse)
    return sims

def parse_folder_by_prefix(sims, basedir, sim_parse=default_prefix_parser, **kwargs):
    rdata = OrderedDict()
    for name, prefix in sims.items():
        simulation = parse_folder(basedir,
                                  sim_parse=default_prefix_parser,
                                  prefix=prefix, **kwargs)
        rdata[name] = simulation
    return rdata

        
def parse_folder(folder, sim_parse=default_name_parser, prefix=None, **kwargs):
    """parse an entire folder to a single dictionary, containing the event data in 'data' field.
    """
    simulation = {}
    events = []
    if prefix:
        fns = glob.glob(folder + "/" + prefix + "*")
    else:
        fns = glob.glob(folder + "/*")
    fns = [pathlib.Path(fn) for fn in fns]
    for fn in fns:
        events += parse(fn, sim_parse=sim_parse, **kwargs)
    df = to_dataframe(events)
    simulation['data'] = df
    simulation['folder'] = folder
    simulation['files'] = fns
    simulation.update(kwargs)
    return simulation

def parse(fn, multisim=True, sim_parse=default_name_parser, slow_read=False):
    """ processes file and returns list of events """
    events = []
    base_fn = os.path.basename(fn)
    if multisim:
        simulation_id = sim_parse(base_fn)
    else:
        simulation_id = 0
    try:
        detector = int(base_fn.split("_")[-1].split("r")[-1])
    except:
        detector = 0
        
    global event_index
    with open(fn) as f:
        header = f.readline() #discard header
        l = f.readline()
        while l:
            try:
                nprim = int(l)
                prims = [f.readline().rstrip() for _ in range(nprim)]
                prims = parse_primaries(prims)
                npix = int(f.readline())
                pix = [f.readline() for _ in range(npix)]
                pix = parse_pixels(pix)
                if(slow_read):
                    valid_pixels = find_valid_events(pix)
                for px in pix:
                    ppxprimid = px[3]
                    if len(prims) == 1:
                        ppxprimid = next(iter(prims))
                    prims[ppxprimid]['total'] += px[2]
                    prims[ppxprimid]['deps'].append(np.array(px))
                    #EXPENSIVE - DISABLE FOR FAST FILE READING
                    if(slow_read):
                        for vpx in valid_pixels:
                            if int(vpx[0]) == int(px[0]) and int(vpx[1]) == int(px[1]):
                                #valid event
                                prims[ppxprimid]['valid'] = True
                                prims[ppxprimid]['vtotal'] = vpx[2]
                for p in prims.values():
                    p['deps'] = np.array(p['deps'])
                    p['total_prims'] = nprim
                    p['detector'] = detector
                    p['simulation'] = simulation_id
                    events.append(p)
                l = f.readline()
            #Should only be necessary for partial files, i.e simulation not yet completed
            except KeyError:
                return events
            except IndexError:
                return events
            except:
                print(sys.exc_info()[0])
                print(sys.exc_info())
                print(fn)
                print(nprim)
                return events
    return events

def get_centroid(deps):
    """ Find centre of mass of pixelated events """
    if deps.any():
        total = np.sum(deps[:,2])
        cx = np.sum(deps[:,0]*deps[:,2])/total
        cy = np.sum(deps[:,1]*deps[:,2])/total
        return np.array([cx, cy])
    else:
        return None
        
def to_dataframe(events):
    """ Convert list of events to pandas dataframe, adds an event centroid location and
    the number of pixels in the event"""
    for e in events:
        e['npix'] = len(e['deps'])
        #e['centroid'] = get_centroid(e['deps'])
    return pd.DataFrame(events)
    


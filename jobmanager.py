import sys
import os
import time
import gashield as ga
import functools
import numpy as np
import athg4_parser as g4

class ActiveJobScheduler(object):
    def __init__(self):
        self.job_count = 0

    def active_jobs(self):
        """
        return list of active jobs
        """
        return []
    
    def submit_job(self, job_cmd):
        """
        submit a job and return a unique identifier
        """
        j = os.popen(job_cmd)
        j.read()
        self.job_count += 1
        return self.job_count
    
    def is_running(self, job_id):
        """
        returns true if job is current running
        """
        return False


## LSF Cluster parsing
def extract_jobid(msg):
    for i in range(len(msg)):
        if msg[i] == "<":
            start = i
        if msg[i] == ">":
            end = i
            break
    return msg[start+1:end]

def parse_job_line(line):
    l = line.split(" ")
    l = [ll for ll in l if ll is not " " and ll is not ""]
    if (len(l) < 5): return None
    return {
        "jobid": l[0],
        "status": l[2],
        "cmd": l[6],
        }

def parse_bjobs_output(output):
    #skip first line
    lines = output.split("\n")
    lines = lines[1:]
    jobs = {}
    for line in lines:
        l = parse_job_line(line)
        if(l):
            jobs[l['jobid']] = l
    return jobs

class LSFJobScheduler(object):
    def __init__(self):
        self.job_count = 0

    def active_jobs(self):
        jobs = os.popen("bjobs -w").read()
        jobs = parse_bjobs_output(jobs)
        if(len(jobs) == 0): return []
        
        return [j for j in jobs]

    def submit_job(self, job_cmd):
        jobtxt = os.popen(job_cmd).read()
        jobid = extract_jobid(jobtxt)
        return jobid
    
    def is_running(self, job_id):
        jobs = self.active_jobs()
        if job_id in jobs:
            return True
        else:
            return False
        

"""
The scheduler interacts with what will run the jobs (e.g ibm cluster)
The generator controls which jobs will be submitted
The JobCoordinator glues them both together
"""
class JobCoordinator(object):
    def __init__(self, scheduler, generator, max_jobs=64):
        self.scheduler = scheduler
        self.generator = generator
        self.max_jobs = max_jobs
        self.jobs = {}
        self.total_jobs = 0

    def finished_jobs(self, actives):
        finished = [job for job in self.jobs.keys() if job not in actives]
        return finished

    def new_request(self):
        """
        return the details of a new job request from the generator
        This should be a dict.
        The dict will be passed back to the generator once the job has finished
        """
        return self.generator.request_details()

    def submit_new_job(self):
        request = self.new_request()
        job_id = self.scheduler.submit_job(request['cmd']) #job is now running

        #create job request
        new_job = {
            "job_id": job_id,
            "start_t": time.time(),
        }
        new_job.update(request)
        self.jobs[job_id] = new_job
        self.total_jobs += 1

    def run(self, max_total_jobs=1e3, wait_time=10):
        while(self.total_jobs < max_total_jobs and not self.generator.done()):
            self.single_pass()
            time.sleep(wait_time)
            
        # clean up remaining jobs
        # todo: cleaner way to do this
        while(self.scheduler.active_jobs()):
            self.single_pass(submit=False)
            time.sleep(wait_time)
        
        print(f"Maximum job limit [{max_total_jobs}] reached")
            
    def single_pass(self, submit=True):
        actives = self.scheduler.active_jobs()
        finished = self.finished_jobs(actives)
        if(submit):
            jobs_to_submit = self.max_jobs - len(actives)
            for i in range(jobs_to_submit):
                self.submit_new_job()
        
        for job_id in finished:
            self.jobs[job_id].update({'run_time': time.time() - self.jobs[job_id]['start_t']})
            self.generator.process(self.jobs[job_id])
            self.log(job_id)
            del self.jobs[job_id]


    def log(self, job_id):
        print(self.jobs[job_id])
        print("\n\n")
        

"""
Job generator generates the string which will launch the job
"""
class JobGenerator(object):
    def __init__(self):
        self.count = 0
        self.pending = {}

    # a job has finished, do some post processing
    def process(self, job):
        print(job['hash'])
        if job['hash'] not in self.pending.keys():
            print("ERROR: JOB NOT FOUND")
        else:
            print(job)
        return

    # send a job to the coordinator
    def request_details(self):
        self.count += 1
        job = {
            "cmd": r"ls -lt",
            "hash": self.count
        }
        self.pending[job['hash']] = job
        return job

    # if no more jobs are required, send true, else false.
    def done(self):
        return False

class GAJobGenerator(object):
    def __init__(self, max_jobs=None):
        self.count = 0
        self.max_jobs = max_jobs
        self.pending = {}
        self.ga = ga.GAOptimizer()

    def process(self, job):
        self.ga.new_result(job['shield'])
        self.count += 1
        
    def request_details(self):
        shield = self.ga.get_new_shield()
        cmd = self.ga.generate_cmd(shield)
        job = {
            'cmd': cmd,
            'hash': ga.get_shield_hash(shield),
            'shield': shield
        }
        self.pending[job['hash']] = job
        return job

    def done(self):
        if not self.max_jobs:
            return False
        return self.count > self.max_jobs

    def resume(self, datadir="data"):
        self.ga.resume(datadir)


class GridSearchGenerator(object):
    def __init__(self, max_jobs=None, matconf={}, config=None):
        self.count = 0
        self.max_jobs = max_jobs
        self.pending = {}
        self.construct_shield_list(matconf)
        self.config = config

    def set_config(self, config):
        self.config = config
        
    def get_output_directory(self, shield):
        return ga.get_directory(shield)
        
    def process(self, job):
        ddir = ga.get_directory(job['shield'])
        ga.get_fitness(job['shield'])


    def request_details(self):
        shield = self.next_shield()
        cmd = self.get_command(shield)
        job = {
            'cmd': cmd,
            'hash': self.get_identifier(shield),
            'shield': shield
        }
        self.pending[job['hash']] = job
        return job
    
    def done(self):
        if not self.max_jobs:
            return False
        return self.count > self.max_jobs

    def get_identifier(self, shield):
        return ga.get_shield_hash(shield)

    def get_command(self, shield):
        executable = self.config['exec']
        source_file = self.config['source_file']
        detector = config['detector']
        hsh = ga.get_shield_hash(shield)
        cmd = f"bsub -oo stdout/{ga.get_shield_hash(shield)}.out "
        cmd += f"-J {hsh} "
        cmd += f"./{executable} -o {ga.get_directory(shield)}/ "
        cmd += f"-s {source_file} "
        cmd += f"--shield {ga.get_shield_file(shield)} "
        cmd += f"-d detector"
        return cmd

    def next_shield(self):
        shield = next(self.shield_list)
        ga.create_directory(shield)
        ga.save_shield(shield)        
        return shield
    
    
    def construct_shield_list(self, matconf):
        mats = matconf['materials']
        th_ranges = matconf['thicknesses']
        max_iter = 1
        for thlist in th_ranges:
            max_iter *= len(thlist)
        indices = [0 for _ in range(len(th_ranges))]
        maxes = [len(th) - 1 for th in th_ranges]
        th_result = []

        #iteratively generate the combinations
        for i in range(max_iter):
            start = len(maxes) - 1
            cur_th = []
            for ind,ths in zip(indices, th_ranges):
                cur_th.append(ths[ind])
            if(np.sum(cur_th) < 4.5):
                th_result.append(cur_th)
                
            indices[-1] += 1
            
            while(indices[start] > maxes[start]):
                indices[start] = 0
                indices[start - 1] += 1
                start -= 1

        print(f"Created {len(th_result)} shields...")
        self.shield_list = []
        for th in th_result:
            shield = {
                "materials": mats,
                "thicknesses": th,
            }
            self.shield_list.append(shield)
        self.max_jobs = len(self.shield_list)
        self.shield_list = iter(self.shield_list)

        #for imat in mats:
            
        
    

#generator = GAJobGenerator()
#generator.resume("/home/jjk/projects/bgsim/lsf-ga-res/data_first/")
#scheduler = LSFJobScheduler()

#controller = JobCoordinator(scheduler, generator, max_jobs=64)
#controller.run(max_total_jobs=1e6, wait_time=100)
config = {
          "exec": "bgsim_gdml",
          "source_file": "spheremodel_source.dat",
          "detector": "detector"
}

# Load the source particles file
with open(config["source_file"], 'r') as f:
    for line in f.readlines():
        v = line.split(" ")
        print(v)
        if(v[-1] == "gcr\n"):
            N_GCR = float(v[-2])
        if(v[-1] == "cxb\n"):
            N_CXB = float(v[-2])

            
print(f"N_GCR = {N_GCR}, N_CXB = {N_CXB}")
R_CXB = 42.2
R_GCR = 4.37
SIM_RADIUS = 9.5
norms = {
    "cxb": N_CXB/(np.pi*(SIM_RADIUS**2)*R_CXB),
    "gcr": N_GCR/(np.pi*(SIM_RADIUS**2)*R_GCR)
}
config["norm"] = norms

t = GridSearchGenerator(matconf={'materials': ["G4_Al", "G4_Mo", "PEEK"],
                                'thicknesses': [np.linspace(3, 4, 5),
                                                np.linspace(0.1, 1, 10),
                                                np.linspace(0.1, 1, 10)]},
                        config=config)


sched = ActiveJobScheduler()
control = JobCoordinator(sched, t, max_jobs=t.max_jobs)
control.run(max_total_jobs = t.max_jobs, wait_time=2000)

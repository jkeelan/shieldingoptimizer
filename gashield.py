import hashlib
import random
import json
import pathlib
import os
import glob
import copy
import glob
import numpy as np
import athg4_parser as g4
# Every material in this list must be declared with the same name
# in ShieldConstructor::DefineMaterials() -> custom_map
MIN_EFFECTIVE_THICKNESS = 2.7*3
MIN_THICKNESS = 2
MAX_THICKNESS = 5
N_LAYERS = 3
S_MATERIAL_IND = 0



materials = ["BCB",
             "PEEK",
             "G4_Al", 
             "G4_Au", 
             "G4_BORON_CARBIDE", 
             "G4_Be", 
             "G4_C", 
             "G4_Cu", 
             "G4_Fe",
             "G4_KAPTON", 
             "G4_Mo", 
             "G4_Ni", 
             "G4_Si", 
             "G4_Ti", 
             "G4_W",
             "G4_Cd",
             "G4_In",
             "G4_Ta",
             "G4_Nb",
             "G4_TEFLON",
]
materials_db = {
    "BCB": {
        "density": 0.957,
        "layers": [1,2],
    },

    "PEEK": {
        "density": 1.32,
        "layers": [1,2],
    },
    "G4_Al": {
        "density": 2.7,
        "layers": [0,1],
    },
    "G4_Si": {
        "density": 2.3,
        "layers": [1,2],
    },
    "G4_BORON_CARBIDE": {
        "density": 2.52,
        "layers": [1,2],
    },
    "G4_Be": {
        "density": 1.85,
        "layers": [1,2],
    },
    "G4_C": {
        "density": 2.26,
        "layers": [1,2],
    },
    "G4_Cu": {
        "density": 8.96,
        "layers": [0,1],
    },
    "G4_Fe": {
        "density": 7.87,
        "layers": [0,1],
    },
    "G4_KAPTON": {
        "density": 1.42,
        "layers": [1,2],
    },
    "G4_Mo": {
        "density": 10.28,
        "layers": [0,1,2],
    },
    "G4_Ti": {
        "density": 4.5,
        "layers": [0,1],
    },
    "G4_W": {
        "density": 19.3,
        "layers": [0,1],
    },
    "G4_TEFLON": {
        "density": 2.2,
        "layers": [1,2],
    },
}
materials_db_keys = materials_db.keys()

def random_material():
    global materials_db_keys
    ind = random.randint(0, len(materials_db_keys) - 1)
    return materials_db_keys[ind]

densities = {
    "G4_Al": 2.7,
    "G4_Si": 2.3,
    "BCB": 0.957,
    "G4_Au": 19.32,
    "G4_BORON_CARBIDE": 2.52,
    "G4_Be": 1.85,
    "G4_C": 2.26,
    "G4_Cu": 8.96,
    "G4_Fe": 7.87,
    "G4_Galactic": 0,
    "G4_KAPTON": 1.42,
    "G4_Mo": 10.28,
    "G4_Ni": 8.9,
    "G4_Ti": 4.5,
    "G4_W": 19.3,
    "PEEK": 1.32,
    "G4_Cd": 8.65,
    "G4_In": 7.31,
    "G4_Ta": 16.69,
    "G4_Nb": 8.57,
    "G4_TEFLON": 2.2,
}

layer_numbers = {
    "G4_Al": [0, 1],
    "G4_Si": [1, 2],
    "BCB": [1, 2],
    "G4_Au": [0, 1],
    "G4_BORON_CARBIDE": [1, 2],
    "G4_Be": [1, 2],
    "G4_C": [1, 2],
    "G4_Cu": [0, 1],
    "G4_Fe": [0, 1],
    "G4_Galactic": [2],
    "G4_KAPTON": [1, 2],
    "G4_Mo": [0, 1],
    "G4_Ni": [0, 1],
    "G4_Ti": [0, 1],
    "G4_W": [0, 1],
    "PEEK": [1, 2],
    "G4_Cd": [0, 1],
    "G4_In": [0, 1],
    "G4_Ta": [0, 1],
    "G4_Nb": [0, 1],
    "G4_TEFLON": [1, 2],
}



def parse_func(*args):
    return 1

config = {
          "exec": "bgsim_gdml",
          "source_file": "spheremodel_source.dat",
          "detector": "detector"
}

# Load the source particles file
with open(config["source_file"], 'r') as f:
    for line in f.readlines():
        v = line.split(" ")
        print(v)
        if(v[-1] == "gcr\n"):
            N_GCR = float(v[-2])
        if(v[-1] == "cxb\n"):
            N_CXB = float(v[-2])

            
print(f"N_GCR = {N_GCR}, N_CXB = {N_CXB}")
R_CXB = 42.2
R_GCR = 4.37
SIM_RADIUS = 9.5
norms = {
    "cxb": N_CXB/(np.pi*(SIM_RADIUS**2)*R_CXB),
    "gcr": N_GCR/(np.pi*(SIM_RADIUS**2)*R_GCR)
}
config["norm"] = norms

def random_shield():
    while(True):
        mats = [random_material() for _ in range(N_LAYERS)]
        ths = [random.random()*MAX_THICKNESS for _ in range(N_LAYERS)]
        shield = {"materials": mats, "thicknesses": ths}
        if(validate_shield(shield)):
            break
    return shield

def get_shield_hash(shield):
    shieldstr = json.dumps(shield).encode("ascii")
    hash_val = hashlib.sha256(shieldstr).hexdigest()[:10]
    return hash_val

def get_shield_file(shield, **args):
    """
    Get the path to the shield json file
    """
    hsh = get_shield_hash(shield)
    fn = hsh + ".json"
    return get_directory(shield, **args) / fn

def get_directory(shield, outputdir="data"):
    """
    Get the directory which contains the shield
    """
    hsh = get_shield_hash(shield)
    path = pathlib.Path(outputdir)
    path = path / hsh
    return path

def create_directory(shield, **args):
    path = get_directory(shield, **args)
    if os.path.exists(path):
        raise RuntimeError(f"<Shield hash collision> in <create_directories>")
    os.makedirs(path)

def save_shield(shield, **args):
    path = get_shield_file(shield, **args)
    shieldstr = json.dumps(shield, indent=4, separators=(',', ': '))
    with open(path, "w") as f:
        f.write(shieldstr)

def check_shield_exists(shield, outputdir="data", **args):
    """
    Test whether the shield hash already exists
    """
    shields = glob.glob(outputdir + "/*")
    shields = [f.split("/")[-1] for f in shields]
    shhash = get_shield_hash(shield)
    if shhash in shields:
        return True
    return False
    

def breed(shield1, shield2):
    """
    Breed two shields together and return the new shield
    """
    div = random.randint(0,N_LAYERS)
    new_shield = {
        "materials": shield1["materials"][:div],
        "thicknesses": shield1["thicknesses"][:div],
    }
    new_shield["materials"] += shield2["materials"][div:]
    new_shield["thicknesses"] += shield2["thicknesses"][div:]
    return new_shield

def mutate_material(shield, mutation_rate=0.01):
    new_shield = copy.deepcopy(shield)
    if(random.random() < mutation_rate):
        ind = random.randint(S_MATERIAL_IND, N_LAYERS - 1)
        new_mat = random_material()
        new_shield["materials"][ind] = new_mat
        
    return new_shield

def mutate_thickness(shield, mutation_rate=0.01):
    new_shield = copy.deepcopy(shield)
    if(random.random() < mutation_rate):
        ind = random.randint(0, N_LAYERS - 1)
        new_th = random.random()*MAX_THICKNESS
        new_shield["thicknesses"][ind] = new_th
        while(sum(new_shield["thicknesses"]) > MAX_THICKNESS):
            new_th = random.random()*MAX_THICKNESS
            new_shield["thicknesses"][ind] = new_th
    return new_shield

def get_counts(ddir,norms):
    emin, emax = 2.0, 7.0
    particles = ["electron", "gamma", "neutron", "proton"]
    sim_prefixes = ["gcr", "cxb"]

    #histogram
    nbins = 50
    data_histogram = np.zeros(nbins)
    bins = np.linspace(emin, emax, nbins + 1)
    bin_widths = (bins[1:] - bins[:-1])
    report = {}
    for sim_prefix in sim_prefixes:
        counts, total = {}, 0
        counts['total'] = 0
        datapack = g4.parse_folder(ddir, sim_parse=parse_func, prefix="_" + sim_prefix)
        if(sim_prefix == "gcr"):
            niel = estimate_niel(datapack)
        df = datapack['data']
        if(len(df) == 0):
            dat = np.zeros(len(bins[1:]))
        else:
            edf = df[(df['total'] > emin) & (df['total'] < emax)]
            dat, be = np.histogram(edf['total'], bins=bins)
        data_histogram += dat/norms[sim_prefix]
        data_histogram /= bin_widths
        if(len(edf) != 0):
            for p in particles:
                counts[p] = len(edf[edf['ptype'] == p])
                total += counts[p]
        else:
            for p in particles:
                counts[p] = 0
                total += 0
                
        counts["total_raw"] = total
        counts["total"] += total/norms[sim_prefix]
        report[sim_prefix] = counts
    report['histogram'] = np.array(list(zip(bins[1:], data_histogram)))
    total = 0
    for source in sim_prefixes:
        total += report[source]['total']
    report['total'] = total
    report['niel'] = niel
    return report

def convert_niel(hist, bins, particle):
    if(particle == "proton"):
        niel_conv = np.loadtxt("niel-curves/protons.dat")
    elif(particle == "neutron"):
        niel_conv = np.loadtxt("niel-curves/neutrons.dat")
    niel_vals = np.interp(bins, niel_conv[:,0], niel_conv[:,1])
    return np.sum(niel_vals*hist)
    

def estimate_niel(gcr):
    # use JPL NIEL curve to estimate the NIEL damage on the device (only protons and neutrons)
    particles = ["proton", "neutron"]
    df = gcr['data']
    bins = np.logspace(-1, 5, 100) #check binning
    niel_val = {}
    total = 0
    for p in particles:
        edf = df[df['ptype'] == p]
        if(len(edf) == 0):
            niel_dose = 0
            niel_val[p] = 0
        else:
            energies = edf['ecur']
            hist, be = np.histogram(energies/1000.0, bins=bins)
            niel_dose = convert_niel(hist, be[1:], p)
            niel_val[p] = niel_dose
        total += niel_dose
    niel_val['total'] = total
    return niel_val

def get_fitness(shield, save=True, delete=True, **args):
    """
    This function takes a shield, finds all the data that was generated
    by the worker node, then calculates the background and returns a dict
    with the total background in the 'total' field.
    It also saves this file to <shield_dir>/report.json
    """
    global config
    norm = config["norm"]
    sdir = get_directory(shield, **args)
    counts = get_counts(str(sdir), norms=norm)
    
    if(save):
        histogram = counts['histogram']
        np.savetxt(sdir / pathlib.Path("histogram.dat"), histogram) #save the histogram
        del counts['histogram'] #delete so not in report
        out = sdir / pathlib.Path("report.json")
        with open(out, "w") as f:
            f.write(json.dumps(counts))
    if(delete): #delete the bg file to save space (could be large, and can always re-run for analysis)
        f = glob.glob(str(sdir) + "/_cxb*") + glob.glob(str(sdir) + "/_gcr*")
        for fn in f:
            os.remove(fn)
        
    return counts

def check_layer_placement(shield):
    mats = shield['materials']
    for i, mat in enumerate(mats):
        valid_layers = materials_db[mat]['layers']
        if i not in valid_layers:
            return False
    return True

def validate_shield(shield):
    # return true if shield meets critera
    total_thickness = sum(shield['thicknesses'])
    total_massdensity = sum([materials_db[m]['density'] * th for m,th in zip(shield["materials"], shield['thicknesses'])])
    if(total_massdensity < MIN_EFFECTIVE_THICKNESS):
        return False
    if(total_thickness < MIN_THICKNESS or total_thickness > MAX_THICKNESS):
        return False
    if(check_shield_exists(shield)):
        return False
    if(not check_layer_placement(shield)):
        return False
    #no duplicates
    mats = set()
    for mat in shield['materials']:
        if mat in mats:
            return False
        else:
            mats.add(mat)
            
    return True
    

# Once a simulation is finished, we generate a new individual from the current population
# and send that to the worker. We then take its new solution and add it to the population,
# performing a survivor test to reduce the total number of the population back to pop_size.

class GAOptimizer(object):
    def __init__(self, pop_size=50, elites=20):
        self.pop_size = pop_size
        self.elites = elites
        self.population = [] #empty
        
    def generate_cmd(self, shield):
        global config
        
        #prepare
        create_directory(shield)
        save_shield(shield)
        
        executable = config["exec"]
        source_file = config["source_file"]
        detector = config["detector"]
        hsh = get_shield_hash(shield)
        cmd = f"bsub -oo stdout/{get_shield_hash(shield)}.out "
        cmd += f"-J {hsh} "
        cmd += f"./{executable} -o {get_directory(shield)}/ "
        cmd += f"-s {source_file} "
        cmd += f"--shield {get_shield_file(shield)} "
        cmd += f"-d detector"
        return cmd

    def get_new_shield(self):
        """
        Return a new shield.
        If the population is less than the desired size, then return a random shield.
        Else use the mu + 1 ga.
        """
        if len(self.population) < self.pop_size:
            self.population.append({"shield": random_shield(), "fitness": 1e6})
            return self.population[-1]['shield']
        else:
            return self.new_shield()

    def get_cumulative_fitness(self):
        fitnesses = [s['fitness'] for s in self.population]
        cumsum = []
        tot = 0
        for f in fitnesses:
            if(f == 0):
                tot += 1e6 # 0 is probably an error, so set default
            else:
                tot += 1/f
            cumsum.append(tot)
        return np.array(cumsum) / sum(cumsum)

    def get_shield_from_distribution(self,cumsum):
        choice = random.random()
        shield_index = 0
        for p in cumsum:
            if choice <= p:
                return shield_index
            shield_index += 1
        return -1        

    def new_shield(self):
        #take two shields from current population
        while(True):
            cum_sh = self.get_cumulative_fitness()
            s1 = self.get_shield_from_distribution(cum_sh)
            s2 = self.get_shield_from_distribution(cum_sh)
            shield1 = self.population[s1]['shield']
            shield2 = self.population[s2]['shield']
            new_shield = breed(shield1, shield2)
            new_shield = mutate_material(new_shield, mutation_rate=0.005)
            new_shield = mutate_thickness(new_shield, mutation_rate=0.05)
            if(validate_shield(new_shield)):
                break
            
        return new_shield

    def new_result(self, shield):
        hsh = get_shield_hash(shield)
        fitness = get_fitness(shield)["total"]
        found = False
        for sh in self.population:
            if(get_shield_hash(sh['shield']) == hsh):
                sh['fitness'] = fitness
                found = True
                break
        if(not found):
            self.population.append({"shield": shield, "fitness": fitness})
        self.sort_population()
        self.cull_population()
        self.log_population()

    def sort_population(self):
        self.population = sorted(self.population, key=lambda x: x['fitness'])

    def cull_population(self):
        self.population = self.population[:self.pop_size]

    def log_population(self, fn="population.txt"):
        with open(fn, "w") as f:
            for p in self.population:
                f.write(json.dumps(p))
                f.write("\n")

    def revive_population(self, outputdir="data"):
        folders = glob.glob(outputdir + "/*")
        resurrected = []
        for folder in folders:
            if os.path.exists(folder + "/report.json"):
                with open(folder + "/report.json", 'r') as f:
                    report = json.load(f)
                shield_hash = folder.split("/")[-1]
                with open(folder + "/" + shield_hash + ".json", 'r') as f:
                    shield = json.load(f)
                resurrected.append({'shield': shield,
                                    'fitness': report['total']
                })
        return resurrected

    def resume(self, outputdir="data"):
        #load all the previously complete shields into the population
        resurrected = self.revive_population(outputdir)
        print(f"{len(resurrected)} survivor(s) were found...")
        self.population += resurrected
        self.sort_population()
        self.cull_population()
        print(f"Initial population is {len(self.population)} after resurrection...")
